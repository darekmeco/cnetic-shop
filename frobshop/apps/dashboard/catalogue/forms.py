import oscar.apps.dashboard.catalogue.forms as base_forms


class ProductClassForm(base_forms.ProductClassForm):
    class Meta(base_forms.ProductClassForm.Meta):
        fields = ['name', 'tax_rate', 'requires_shipping', 'track_stock', 'options']
