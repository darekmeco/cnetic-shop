from oscar.apps.shipping import repository
from . import methods


class Repository(repository.Repository):
    methods = (methods.FixedPrice(16, 23, 'Kurier DHL'), methods.Free())
