import oscar.apps.checkout.apps as apps
from django.urls import path
from oscar.core.loading import get_class


class CheckoutConfig(apps.CheckoutConfig):
    name = 'apps.checkout'

    def get_urls(self):
        self.notify_view = get_class('checkout.views', 'NotifyView')
        self.return_from_payment_view = get_class('checkout.views', 'ReturnFromPaymentView')

        urls = super().get_urls() + [
            path('notify', self.notify_view.as_view(), name='notify'),
            path('return-from-payment', self.return_from_payment_view.as_view(), name='return_from_payment')
        ]

        return self.post_process_urls(urls)
