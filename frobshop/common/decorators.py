from functools import wraps

from django.http import HttpResponseNotAllowed
from django.utils.log import log_response


def require_source_ip(ip_list):
    def decorator(func):
        @wraps(func)
        def inner(request, *args, **kwargs):
            ip = request.META.get('REMOTE_ADDR')
            if ip not in ip_list:
                response = HttpResponseNotAllowed(ip_list)
                log_response(
                    'IP Address Not Allowed (%s): %s', request.method, request.path,
                    response=response,
                    request=request,
                )
                return response
            return func(request, *args, **kwargs)

        return inner

    return decorator
