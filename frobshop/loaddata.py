from django.core import management
from django.core.management.commands import loaddata
management.call_command('loaddata', 'fixtures/child_products.json', verbosity=1)


	# sandbox/manage.py loaddata sandbox/fixtures/child_products.json
	# sandbox/manage.py oscar_import_catalogue sandbox/fixtures/*.csv
	# sandbox/manage.py oscar_import_catalogue_images sandbox/fixtures/images.tar.gz
	# sandbox/manage.py oscar_populate_countries --initial-only
	# sandbox/manage.py loaddata sandbox/fixtures/pages.json sandbox/fixtures/ranges.json sandbox/fixtures/offers.json
	# sandbox/manage.py loaddata sandbox/fixtures/orders.json
	# sandbox/manage.py clear_index --noinput
	# sandbox/manage.py update_index catalogue
	# sandbox/manage.py thumbnail cleanup
	# sandbox/manage.py collectstatic --noinput