from decimal import Decimal as D
from typing import Type

from oscar.apps.catalogue.models import Product
from oscar.apps.partner import strategy
from oscar.apps.partner.strategy import UseFirstStockRecord, StockRequired, FixedRateTax, Structured, \
    TaxInclusiveFixedPrice, UnavailablePrice


class Selector(strategy.Selector):
    """
    Responsible for returning the appropriate strategy class for a given
    user/session.

    This can be called in three ways:

    #) Passing a request and user. This is for determining
       prices/availability for a normal user browsing the site.

    #) Passing just the user. This is for offline processes that don't
       have a request instance but do know which user to determine prices for.

    #) Passing nothing. This is for offline processes that don't
       correspond to a specific user, e.g., determining a price to store in
       a search index.

    """

    def strategy(self, request=None, user=None, **kwargs):
        """
        Return an instantiated strategy instance
        """
        # Default to the backwards-compatible strategy of picking the first
        # stockrecord but charging zero tax.
        return strategy.UK(request)


class ProductTypeTax(object):
    def parent_pricing_policy(self, product, children_stock):
        stockrecords = [x[1] for x in children_stock if x[1] is not None]
        if not stockrecords:
            return UnavailablePrice()

    def pricing_policy(self, product: Type[Product], stockrecord):
        """

        :type product: Product
        """
        return TaxInclusiveFixedPrice(
            currency=stockrecord.price_currency,
            excl_tax=stockrecord.price_excl_tax,
            tax=product.product_class.tax_rate)


class Poland(UseFirstStockRecord, StockRequired, ProductTypeTax, Structured):
    """
    Sample strategy for the UK that:

    - uses the first stockrecord for each product (effectively assuming
        there is only one).
    - requires that a product has stock available to be bought
    - applies a fixed rate of tax on all products

    This is just a sample strategy used for internal development.  It is not
    recommended to be used in production, especially as the tax rate is
    hard-coded.
    """
    # Use UK VAT rate (as of December 2013)
    rate = D('0.23')
