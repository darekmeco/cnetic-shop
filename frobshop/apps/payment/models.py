from django.db import models


class PaymentNotify(models.Model):
    content = models.TextField(null=True)
    headers = models.TextField(null=True)
    meta = models.TextField(null=True)
    sign = models.CharField(max_length=255, null=True)
    gensign = models.CharField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


from oscar.apps.payment.models import *  # noqa isort:skip
