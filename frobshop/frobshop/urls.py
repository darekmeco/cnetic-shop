from django.apps import apps
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from django.contrib import admin
from oscar.views import handler403, handler404, handler500

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),

    # The Django admin is not officially supported; expect breakage.
    # Nonetheless, it's often useful for debugging.

    path('admin/', admin.site.urls),
    path('', include(apps.get_app_config('core').urls[0])),
]


if settings.DEBUG:
    # import debug_toolbar

    # Server statics and uploaded media
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    # Allow error pages to be tested
    urlpatterns += [
        path('403', handler403, {'exception': Exception()}),
        path('404', handler404, {'exception': Exception()}),
        path('500', handler500),
        # path('__debug__/', include(debug_toolbar.urls)),
    ]