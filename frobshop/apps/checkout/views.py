import json
from hashlib import md5
from typing import List

import requests
from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse, HttpResponse
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from oscar.apps.basket.models import Basket, Line
from oscar.apps.catalogue.models import Product
from oscar.apps.checkout import views, signals
from oscar.apps.checkout.utils import CheckoutSessionData
from oscar.apps.checkout.views import OrderPlacementMixin, logger, UnableToPlaceOrder, Order
from oscar.apps.order.models import ShippingAddress
from oscar.apps.payment import models
from oscar.apps.payment.exceptions import RedirectRequired
from phonenumber_field.phonenumber import PhoneNumber
from django.utils.translation import gettext as _

from apps.payment.models import PaymentNotify
from common.decorators import require_source_ip


@method_decorator(csrf_exempt, name='dispatch')
class NotifyView(generic.View):
    preview = False
    model = PaymentNotify

    @method_decorator(require_source_ip(settings.PAYU_SAFE_IPS))
    def post(self, request, *args, **kwargs):
        body = json.loads(request.body)
        headers = json.dumps(str(request.headers))
        meta = json.dumps(str(request.META))
        http_sign = request.META.get('HTTP_OPENPAYU_SIGNATURE')

        try:
            signature = next(filter(lambda r: r[0] == 'signature', [i.split('=') for i in http_sign.split(';')]))[1]
        except KeyError:
            return HttpResponse(status=400)

        line = request.body + settings.PAYU_SECOND_KEY.encode('utf-8')
        generated_signature = md5(line).hexdigest()

        if signature != generated_signature:
            return HttpResponse(status=403)

        try:
            PaymentNotify.objects.create(content=request.body, headers=headers, meta=meta, sign=signature,
                                         gensign=generated_signature)
            order_number = body['order']['extOrderId']
            order = Order._default_manager.get(number=order_number)
            statuses = order.available_statuses()
            order.set_status('Paid')

            return JsonResponse({'status': 'success'})
        except Exception as e:
            logger.exception("Order #%s: unhandled exception while placing order (%s)", order_number, e)


class ReturnFromPaymentView(OrderPlacementMixin, generic.TemplateView):
    pre_conditions = [
        'check_basket_is_not_empty',
        'check_basket_is_valid',
        'check_user_email_is_captured',
        'check_shipping_data_is_captured',
        'check_payment_data_is_captured'
    ]

    template_name = 'oscar/checkout/payment_details.html'
    template_name_preview = 'oscar/checkout/preview.html'

    def get_template_names(self):
        return [self.template_name_preview] if self.preview else [
            self.template_name]

    def render_preview(self, request, **kwargs):
        """
        Show a preview of the order.

        If sensitive data was submitted on the payment details page, you will
        need to pass it back to the view here so it can be stored in hidden
        form inputs.  This avoids ever writing the sensitive data to disk.
        """
        self.preview = True
        ctx = self.get_context_data(**kwargs)
        return self.render_to_response(ctx)

    def get_pre_conditions(self, request):
        self.restore_frozen_basket()
        return super().get_pre_conditions(request)

    def get(self, request: WSGIRequest, *args, **kwargs):

        ctx = self.get_context_data()
        order_kwargs = ctx['order_kwargs']
        payment_kwargs = ctx['payment_kwargs']

        try:
            signals.post_payment.send_robust(sender=self, view=self)
            basket = request.basket
            user = request.user
            session = self.checkout_session
            order_number = session.get_order_number()
            shipping_address = self.get_shipping_address(basket)
            billing_address = self.get_billing_address(shipping_address=shipping_address)
            shipping_method = self.get_shipping_method(basket=basket, shipping_address=shipping_address)
            shipping_charge = shipping_method.calculate(basket)
            order_total = self.get_order_totals(basket, shipping_method.calculate(basket))

            # Payment successful! Record payment source
            source_type, __ = models.SourceType.objects.get_or_create(name="PayU")

            source = models.Source(source_type=source_type, amount_allocated=order_total.incl_tax,
                                   reference=order_number)
            self.add_payment_source(source)
            # Record payment event
            self.add_payment_event('pre-paid', order_total.incl_tax)

            # totals = self
            # If all is ok with payment, try and place order
            logger.info("Order #%s: payment successful, placing order")
            return self.handle_order_placement(
                order_number, user, basket, shipping_address, shipping_method,
                shipping_charge, billing_address, order_total, **order_kwargs)
        except UnableToPlaceOrder as e:
            # It's possible that something will go wrong while trying to
            # actually place an order.  Not a good situation to be in as a
            # payment transaction may already have taken place, but needs
            # to be handled gracefully.
            msg = str(e)
            logger.error("Order #%s: unable to place order - %s", order_number, msg, exc_info=True)
            self.restore_frozen_basket()
            return self.render_preview(self.request, error=msg, **payment_kwargs)

        except Exception as e:
            # Hopefully you only ever reach this in development
            logger.exception("Order #%s: unhandled exception while placing order (%s)", order_number, e)
            error_msg = _("A problem occurred while placing this order. Please contact customer services.")
            self.restore_frozen_basket()
            return self.render_preview(self.request, error=error_msg, **payment_kwargs)


# Subclass the core Oscar view so we can customise
class PaymentDetailsView(views.PaymentDetailsView):
    preview = True
    template_name = 'oscar/checkout/payment_details.html'

    def handle_payment(self, order_number, total, **kwargs):
        # Talk to payment gateway.  If unsuccessful/error, raise a
        # PaymentError exception which we allow to percolate up to be caught
        # and handled by the core PaymentDetailsView.
        url = 'https://secure.snd.payu.com/pl/standard/user/oauth/authorize'
        auth_response = requests.post(url, data={'grant_type': 'client_credentials', 'client_id': settings.PAYU_POS_ID,
                                                 'client_secret': settings.PAYU_CLIENT_SECRET})
        response = auth_response.json()

        logger.info(response);

        # Payment successful! Record payment source
        source_type, __ = models.SourceType.objects.get_or_create(
            name="PayU")
        source = models.Source(
            source_type=source_type,
            amount_allocated=total.incl_tax,
            reference=order_number)
        self.add_payment_source(source)
        # Record payment event
        self.add_payment_event('pre-auth', total.incl_tax)

        request = self.request

        meta = json.dumps(str(request.META));
        headers = json.dumps(str(request.headers))

        url = 'https://secure.snd.payu.com/api/v2_1/orders'

        ctx = self.get_context_data();
        address = self.get_default_billing_address()

        shipping: ShippingAddress = self.get_shipping_address(request.basket)
        phone: PhoneNumber = shipping.phone_number
        basket: Basket = request.basket;
        lines = basket.all_lines();

        products: List = []
        line: Line
        for line in lines:
            product: Product = line.product
            products.append(dict(name=product.title, unitPrice=int(line.price_incl_tax * 100), quantity=line.quantity))

        if isinstance(phone, PhoneNumber):
            phone = phone.as_e164

        data = json.dumps({
            "notifyUrl": self.request.build_absolute_uri(reverse('checkout:notify')),
            'continueUrl': self.request.build_absolute_uri(reverse('checkout:return_from_payment')),
            "customerIp": request.META.get("REMOTE_ADDR"),
            "merchantPosId": settings.PAYU_POS_ID,
            "description": "Cnetic Shop",
            "currencyCode": "PLN",
            "totalAmount": int(total.incl_tax * 100),
            "extOrderId": order_number,
            "buyer": {
                "email": request.user.email,
                "phone": phone,
                "firstName": shipping.first_name,
                "lastName": shipping.last_name,
                "language": "pl"
            },
            "products": products
        })

        token = response['access_token']
        payment_response = requests.post(url,
                                         data=data,
                                         headers={
                                             'Authorization': f'Bearer {token}',
                                             "Content-Type": "application/json"
                                         })

        logger.info(payment_response.text);

        print(payment_response.url)
        print(payment_response.status_code)
        print(payment_response.text)

        if payment_response.url is not None:
            raise RedirectRequired(payment_response.url)
