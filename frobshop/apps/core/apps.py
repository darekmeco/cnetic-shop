from django.conf import settings
from django.conf.urls import url
from django.urls import path
from django.utils.translation import gettext_lazy as _
from django.views.generic import RedirectView
from oscar import config
from oscar.core.loading import get_class


class CoreConfig(config.Shop):
    label = 'core'
    name = 'apps.core'
    verbose_name = _('Core')
    namespace = 'core'

    def ready(self):
        super().ready()
        self.main_view = get_class('core.views', 'MainView')

    def get_urls(self):
        urls = super().get_urls()
        urls = [
            path('', self.main_view.as_view(), name='index')
        ] + urls
        return self.post_process_urls(urls)
