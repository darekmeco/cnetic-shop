from django.db import models
from django.utils.translation import gettext_lazy as _

from oscar.apps.catalogue.abstract_models import AbstractProductClass


def default_tax_rate() -> int:
    return 0


class ProductClass(AbstractProductClass):
    tax_rate = models.IntegerField(_("Tax rate"), default=default_tax_rate)


from oscar.apps.catalogue.models import *  # noqa isort:skip
